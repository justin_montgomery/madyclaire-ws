<%@tag description="Footer Tag" pageEncoding="UTF-8"%>
<div class="footer" style="background-color:#eee; color:#989696; border-top: 1px solid black;">
	<div class="container">
		<div style="text-align:center;" class="footer-top-at">
			<div id="newsletter" class="col-md-4 amet-sed">
				<h4>Newsletter</h4>
				<p>Would you like notifications about promotions and discounts? 
				Provide your email and we'll keep you updated without sharing your info to third parties.</p>
				<input id="emailAddress" style="border: 1px solid grey; border-radius:0.3em;" type="text" name="emailAddress" placeholder="email address"/>
				<input style=" border-radius:0.3em;" id="newsletter-enroll-button" type="submit" value="Enroll"/>
			</div>
			<div id="follow-us" class="col-md-4 amet-sed ">
				<h4>Follow Us</h4>

				<p>Click to view our 
					<a style="color:#989696; text-decoration:underline;" href="${url}/blog" 
					onMouseOver="this.style.color='#EF5F21'"
   					onMouseOut="this.style.color='#989696'">blog</a>
   					 or follow us on social media:</p>
				<ul style="display:block;" class="social">
					<li><a title="Facebook" href="https://facebook.com/madyclairestore" target="_blank"><img src="/images/facebook.png"/></a></li>
					<li><a title="Instagram" href="https://instagram.com/madyclairestore" target="_blank"><img src="/images/instagram.png"/></a></li>
					<li><a title="Pinterest" href="https://pinterest.com/madyclairestore" target="_blank"><img src="/images/pinterest.png"/></a></li>
					<li><a title="Twitter" href="https://twitter.com/madyclairestore" target="_blank"><img src="/images/twitter.png"/></a></li>
				</ul>
			</div>
			<div id="moreinfo-faqs" class="col-md-4 amet-sed">
				<h4>© 2016-2017 Mady Claire</h4>
				<ul class="nav-bottom">
					<li><a href="mailto:info@madyclaire.com">Contact Us</a></li>
					<li><a href="/about">About Us</a></li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
