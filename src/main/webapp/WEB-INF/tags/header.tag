<%@tag description="Header Tag" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table style="background-color:#eee; width: 100%; padding-left: 5%; padding-right: 5%; padding-top:2%;"
	class="header">
	 <colgroup>
       <col span="1" style="width: 33%;"/>
       <col span="1" style="width: 33%;"/>
       <col span="1" style="width: 33%;"/>
    </colgroup>
    <tbody>
		<tr style="border-collapse: collapse;" id="header-first-row">
			<td style="padding-left: 5%; padding-top:1%; height: 5%;" colspan="2" class="header-cell">
				<form action="/search">
					<!-- /shop/MadyClaireStore?ref=l2-shopheader-name&search_query=  +words-->
					<input style="border: 1px solid grey; border-radius:0.3em; width:40%;margin-top:0.2em;" type="text" placeholder="search..."/> 
					<input style="padding-top:0.1em; padding-bottom:0.1em;" class="btn btn-default" id="search-button" type="submit" value="Go" />
				</form>
			</td>
			<td style="height:5%; white-space:nowrap;" class="header-cell">
				<span> Premium apparel. Affordable prices.</span>
			</td>
			<!--  
			<td style="height: 5%;white-space:nowrap;" class="header-cell">
				<table style="width:100%;">
					<tr>
						<td style="width:50%; text-align:right;"><a style="text-decoration:none;" href="/account">ACCOUNT</a></td>
						<td style="width:50%; text-align:center;"><a style="overflow:hidden; text-decoration:none;" href="/checkout">
								<span style="overflow:hidden; display:inline;" class="total">
									<span style="overflow:hidden; display:inline;" id="simpleCart_quantity" class="simpleCart_quantity">CART (${cartQuantity})</span>
									<img style="width: 7%; height: auto; display:inline; overflow:hidden;" src="/images/cart.png"/>
								</span> 
						</a></td>
					</tr>
				</table>
			</td>
			-->
		</tr>
		<tr id="header-second-row">
			<td style="text-align: center; padding: 2%;" colspan="3"
				class="header-cell"><a id="logo" href="${url}/"><img style="width:30%; height:auto;"src="/images/logo.png" alt="Home"></a></td>
		</tr>
		<tr id="header-third-row">
			<td style="text-align: center; padding-left: 5%; padding-right: 5%; padding-top:2%; padding-bottom: 2%;" colspan="3">
				<table style="padding: 5%; width:100%; text-transform: capitalize; font-size:1.2em; border-top: 1px solid grey; border-bottom: 1px solid grey;">
					<tr>
						<td style="padding-top:1%; padding-bottom:1%;width:33%;"><a style="text-decoration:none;" 
							href="http://etsy.com/people/MadyClaireStore" target="_blank">shop</a></td>
						<td style="width:33%;"><a style="text-decoration:none;" href="/shop/custom">custom</a></td>
						<td style="width:33%;"><a style="text-decoration:none;" href="/blog">blog</a></td>
					</tr>
				</table>
			</td>
		</tr>
		<c:if test="${not empty statusMessage}">
			<tr>
				<td style="text-align:center; padding-top:2%; padding-bottom:2%; color:blue;">
					<p>${statusMessage}</p>
				</td>
			</tr>
 		</c:if>
	</tbody>
</table>