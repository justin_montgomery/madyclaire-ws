<%@tag description="Simple Wrapper Tag" pageEncoding="UTF-8"%>
<%@taglib prefix="fragment" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="includes" fragment="true" required="false" %>

<!DOCTYPE html>
<c:set var="req" value="${pageContext.request}" />
<c:set var="url" value="${req.scheme}://${req.serverName}:${req.serverPort}" /><html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mady Claire</title>
    <link rel="shortcut icon" type="image/x-icon" href="/images/favicon.ico" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="/css/main.css" />
	<link rel="stylesheet" href="/css/flexslider.css"/>
	<link rel="stylesheet" href="/css/memenu.css"/>
	<link rel="stylesheet" href="/css/style.css"/>
	<!-- google -->
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<script src="/js/google-scripts.js"></script>
  	<script src="/js/newsletter-enroll.js"></script>
	<jsp:invoke fragment="includes"/>
</head>
<body>
	<fragment:header/>
	<div class="content" style="padding-left:5%; padding-right:5%; padding-top:0%; padding-bottom:3%;">
	  	<jsp:doBody/>
	</div>
  	<fragment:footer/>
</body>
</html>