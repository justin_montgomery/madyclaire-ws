<%@taglib prefix="layout" tagdir="/WEB-INF/tags"%>
<layout:main-wrapper>
	<jsp:attribute name="includes">
		<script>
		$(document).ready(function(){
		    $("#register-button").click(function(){
		    	console.debug("hiding login form.");
		        $("#login-form").hide();
		        console.debug("showing registration form");
		        $("#register-form").show();
		    });
		});
		</script>
	</jsp:attribute>
	<jsp:body>
		<h3 style="color:black;margin-bottom:1em;border-bottom:1px solid grey; text-align:center;">Customer Sign In</h3>
		
		<!--  LOG IN FORM -->
		<form id="login-form" action="/account/login" method="post">
			<table style="width:50%;">
				<tr>
					<td>
						<div class="form-group">
							<label for="usr">Email:</label><input size="20" name="emailAddress" type="text" class="form-control" id="usr">
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="form-group">
							<label for="pwd">Password:</label> <input name="password" type="password" class="form-control" id="pwd">
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<input style="padding-top:0.1em; padding-bottom:0.1em;" 
							size="20" class="btn btn-default" type="submit" value="Sign In" />
					</td>
				</tr>
				<tr>
					<td style="padding-top:5%; padding-bottom:5%;">New to Mady Claire?</td>
				</tr>
				<tr>
					<td>
						<input style="padding-top:0.1em; padding-bottom:0.1em;" id="register-button"
							size="20" class="btn btn-default" type="button" value="Register" />
					</td>
				</tr>
			</table>
		</form>
		
		<!--  REGISTER FORM -->
		<form id="register-form" action="/account/register" style="display:none;" method="post">
			 <table style="width:50%;">
				<tr>
					<td>
						<div class="form-group">
							<label for="usr">Name:</label><input name="name" type="text" class="form-control" id="fn">
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="form-group">
							<label for="usr">Email:</label><input name="emailAddress" type="text" class="form-control" id="usr">
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="form-group">
							<label for="pwd">Password:</label> <input name="password" type="password" class="form-control" id="pwd">
						</div>
						<div class="form-group">
							<label for="pwd">Confirm Password:</label> <input name="passwordConfirm" type="password" class="form-control" id="pwd">
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<input style="padding-top:0.1em; padding-bottom:0.1em;" 
							size="20" class="btn btn-default" type="submit" value="Register" />
					</td>
				</tr>
			</table>
		</form>
	</jsp:body>
</layout:main-wrapper>