<%@taglib prefix="layout" tagdir="/WEB-INF/tags"%>
<layout:main-wrapper>
	<jsp:attribute name="includes">
		<script type="text/javascript">
			$(function() {
				$("#slider").responsiveSlides({
					auto : true,
					nav : true,
					speed : 500,
					namespace : "callbacks",
					pager : true,
				});
			});
		</script>
		<script src="/js/responsiveslides.min.js" type="text/javascript"></script>
	</jsp:attribute>
	<jsp:body>
		<div class="content-top" style="padding-top:1%;">
		<div class="grid-in">
			<div class="col-md-4 grid-top">
				<a href="single.html" class="b-link-stripe b-animate-go  thickbox"><img
						class="img-responsive" src="/images/crib.jpg" alt="">
					<div class="b-wrapper">
						<h3 class="b-animate b-from-left b-delay03 " style="height: 100%;">
							<span>Baby Outfits</span>	
						</h3>
					</div>
				</a>
			</div>
			<div class="col-md-4 grid-top">
				<a href="single.html" class="b-link-stripe b-animate-go  thickbox"><img
						class="img-responsive" src="/images/crib.jpg" alt="">
					<div class="b-wrapper">
						<h3 class="b-animate b-from-left b-delay03 " style="height: 100%;">
							<span>Baby Bedding</span>	
						</h3>
					</div>
				</a>
			</div>
			<div class="col-md-4 grid-top">
				<a href="single.html" class="b-link-stripe b-animate-go  thickbox"><img
						class="img-responsive" src="/images/crib.jpg" alt="">
					<div class="b-wrapper">
						<h3 class="b-animate b-from-left    b-delay03 "	style="height: 100%;">
							<span>Crochet Accessories</span>	
						</h3>
					</div>
				</a>
			</div>
			<div class="clearfix"> </div>
		</div>
		<div class="grid-in">
			<div class="col-md-4 grid-top">
				<a href="single.html" class="b-link-stripe b-animate-go  thickbox"><img
						class="img-responsive" src="/images/crib.jpg" alt="">
					<div class="b-wrapper">
						<h3 class="b-animate b-from-left    b-delay03 "
					style="height: 100%;">
							<span>Custom Outfits</span>	
						</h3>
					</div>
				</a>
			</div>
			<div class="col-md-4 grid-top">
				<a href="single.html" class="b-link-stripe b-animate-go  thickbox"><img
						class="img-responsive" src="/images/crib.jpg" alt="">
					<div class="b-wrapper">
						<h3 class="b-animate b-from-left    b-delay03 "
					style="height: 100%;">
							<span>Custom Accessories</span>	
						</h3>
					</div>
				</a>
			</div>
			<div class="col-md-4 grid-top">
				<a href="single.html" class="b-link-stripe b-animate-go  thickbox"><img
						class="img-responsive" src="/images/crib.jpg" alt="">
					<div class="b-wrapper">
						<h3 class="b-animate b-from-left    b-delay03 "
					style="height: 100%;">
							<span>Vinyl Cutouts</span>	
						</h3>
					</div>
				</a>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	</jsp:body>
</layout:main-wrapper>
