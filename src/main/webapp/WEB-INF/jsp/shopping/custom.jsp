<%@taglib prefix="layout" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<layout:main-wrapper>
<jsp:attribute name="includes">
<script type="text/javascript">
$(document).ready(function() {

	$("#submit-custom-request").click( function(event) {
	    var message = $("#message-field").val();
	    var name = $("#name-field").val();
		if(!name || name.length < 3){
			alert("Please add your name so we can better address your request.");
			event.preventDefault();
			return;
		}
		if(!message || message.length < 20){
			alert("Please fill out the request details before submitting a request.");
			event.preventDefault();
			return;
		}
	    var href = $(this).attr('href');
	    $(this).attr('href', href + name + "&body="+message);
	});
	//&body=
});
</script>
</jsp:attribute>
<jsp:body>
	<c:if test="${empty customRequest}">
		<div id="custom-request-form" class="blog" style="padding-bottom:0%;">
			<div class="container">
				<div class="blog-top">
					<div  class=" grid_3 grid-1">
						<h3 style="text-align:center;font-weight:bold;">We Love New Projects</h3>
						<p style="padding-top:2%; width:75%;margin-left:12%;margin-right:12%;">We're always dreaming of new ways to create products that make people smile.  If you have
						a custom baby outfit, bedding, accessory, or vinyl sticker in mind, let us know!  Please be as 
						detailed as possible.  If you have specific fabrics or designs in mind, you can paste links to those in your message.</p>
						
					</div>
					<div class="single-bottom">
						<h3 style="font-size:medium;">Request Custom Items</h3>
						<form action="/shop/custom" method=POST>
							<input id="name-field" type="text" name="name" placeholder="Name">
								<!-- 
							<input id="email-field"
								type="text" name="email" value="Email" onfocus="this.value='';"
								onblur="if (this.value == '') {this.value ='Email';}"> 
							<input id="subject-field"
								type="text" name="subject" value="Subject"
								onfocus="this.value='';"
								onblur="if (this.value == '') {this.value ='Subject';}">
								-->
							<textarea id="message-field" name="message" cols="77" rows="6" value=" " placeholder="Request Details"></textarea>
							<a class = "btn btn-success" id="submit-custom-request" href="mailto:info@madyclaire.com?subject=Custom Request for ">Request</a>
						</form>
					</div>
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${customRequest}">
		<div class="blog">
			<div class="container">
				<div class="blog-top">
					<div class=" grid_3 grid-1">
						<h3>Thank you!</h3>
						
						<p>We'll get back to you as soon as possible. 
						You can expect either an estimate or request clarification in the next few business days.</p>
						
						<img src="/images/crib.jpg" class="img-responsive" alt=""/>
					</div>
				</div>
			</div>
		</div>
	</c:if>
</jsp:body>
</layout:main-wrapper>