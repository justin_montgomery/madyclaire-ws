<%@taglib prefix="layout" tagdir="/WEB-INF/tags"%>
<layout:main-wrapper>
	<div class="container" style="padding-top:2%;">
		<div class="blog-top">
			<div class=" grid_3 grid-1">
				<h3 style="text-transform:capitalize;"">
					Who are we?
				</h3>
				<a style="float:left; padding-top:1%;padding-right:1%;" href="blog_single.html"><img src="/images/crib.jpg"
					class="img-responsive" alt=""></a>

				<p style="padding-top:1%;">Mady Claire is a small family owned business operating out of East Tennessee. We named our business after our baby girl
				 because she's become our muse. Everything we craft is because we once had the thought that it would be great for her.  Our family
				 has been crafting for many generations, but we've decided to turn our hobby into a little spare change (and hopefully make you happy
				 in the process!).</p>
			</div>
		</div>
	</div>
</layout:main-wrapper>