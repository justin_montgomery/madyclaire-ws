<%@taglib prefix="layout" tagdir="/WEB-INF/tags"%>
<layout:main-wrapper>
<div class="blog">
<div class="container">
	<h1>Recent Posts</h1>
	       <div class="blog-top">
			  <div class="col-md-6 grid_3">
					<h3 style="margin-bottom:0;"><a href="/blog/${blogId}">Title</a></h3>
					<div class="blog-poast-info">
						<i class="date">12-04-2015</i>
							<!--  <li><a class="p-blog" href="#"><i class="comment"> </i>No Comments</a></li>-->
				    </div>
					<a href="/blog/${blogId}"><img src="/images/crib.jpg" class="img-responsive" alt=""> picture</a>
					
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					<div class="button"><a href="blog_single.html">Read More</a></div>
				</div>
				<div class="col-md-6 grid_3">
					<h3><a href="blog_single.html">Lorem Ipsum is simply</a></h3>
					<a href="blog_single.html"><img src="images/b2.jpg" class="img-responsive" alt=""></a>
					
					<div class="blog-poast-info">
						<ul>
							<li><a class="admin" href="#"><i> </i> Admin </a></li>
							<li><span><i class="date"> </i>12-04-2015</span></li>
							<li><a class="p-blog" href="#"><i class="comment"> </i>No Comments</a></li>
						</ul>
				    </div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					<div class="button"><a href="blog_single.html">Read More</a></div>
				</div>
				<div class="clearfix"> </div>
			  </div>
			 
			  <div class="blog-top">
			  <div class="col-md-6 grid_3">
					<h3><a href="blog_single.html">Lorem Ipsum is simply</a></h3>
					<a href="blog_single.html"><img src="images/b3.jpg" class="img-responsive" alt=""></a>
					
					<div class="blog-poast-info">
						<ul>
							<li><a class="admin" href="#"><i> </i> Admin </a></li>
							<li><span><i class="date"> </i>12-04-2015</span></li>
							<li><a class="p-blog" href="#"><i class="comment"> </i>No Comments</a></li>
						</ul>
				    </div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					<div class="button"><a href="blog_single.html">Read More</a></div>
				</div>
				<div class="col-md-6 grid_3">
					<h3><a href="blog_single.html">Lorem Ipsum is simply</a></h3>
					<a href="blog_single.html"><img src="images/b4.jpg" class="img-responsive" alt=""></a>
					
					<div class="blog-poast-info">
						<ul>
							<li><a class="admin" href="#"><i> </i> Admin </a></li>
							<li><span><i class="date"> </i>12-04-2015</span></li>
							<li><a class="p-blog" href="#"><i class="comment"> </i>No Comments</a></li>
						</ul>
				    </div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					<div class="button"><a href="blog_single.html">Read More</a></div>
				</div>
				<div class="clearfix"> </div>
			  </div>
			  <!---728x90--->
			  <div class="blog-bottom">
			  <div class="col-md-6 grid_3">
					<h3><a href="blog_single.html">Lorem Ipsum is simply</a></h3>
					<a href="blog_single.html"><img src="images/b5.jpg" class="img-responsive" alt=""></a>
					
					<div class="blog-poast-info">
						<ul>
							<li><a class="admin" href="#"><i> </i> Admin </a></li>
							<li><span><i class="date"> </i>12-04-2015</span></li>
							<li><a class="p-blog" href="#"><i class="comment"> </i>No Comments</a></li>
						</ul>
				    </div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					<div class="button"><a href="blog_single.html">Read More</a></div>
				</div>
				<div class="col-md-6 grid_3">
					<h3><a href="blog_single.html">Lorem Ipsum is simply</a></h3>
					<a href="blog_single.html"><img src="images/b1.jpg" class="img-responsive" alt=""></a>
					
					<div class="blog-poast-info">
						<ul>
							<li><a class="admin" href="#"><i> </i> Admin </a></li>
							<li><span><i class="date"> </i>12-04-2015</span></li>
							<li><a class="p-blog" href="#"><i class="comment"> </i>No Comments</a></li>
						</ul>
				    </div>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					<div class="button"><a href="blog_single.html">Read More</a></div>
				</div>
				<div class="clearfix"> </div>
			  </div>
      </div>
</div>
</layout:main-wrapper>