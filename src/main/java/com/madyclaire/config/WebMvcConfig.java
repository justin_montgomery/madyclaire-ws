package com.madyclaire.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.madyclaire.RequestInterceptor;

@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {

  @Autowired 
  private RequestInterceptor requestInterceptor;

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(requestInterceptor); //can optionally add URL paths
  }
}