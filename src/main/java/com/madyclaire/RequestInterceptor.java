/**
 * 
 */
package com.madyclaire;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author jwm
 *
 */
@Component
public class RequestInterceptor implements HandlerInterceptor{

	private Logger log = Logger.getLogger(getClass());
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		log.info(" pre handle remote host : " + request.getRemoteHost());
		return true;
	}//end prehandle

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, 
			ModelAndView modelAndView) throws Exception {
		
		/*
		 * if(modelAndView.getModel().get("cartQuantity") == null){
			modelAndView.getModel().put("cartQuantity", 0);
		}
		 */
		
	}//end posthandle

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		
	}//end afterCompletion

}//end interceptor
