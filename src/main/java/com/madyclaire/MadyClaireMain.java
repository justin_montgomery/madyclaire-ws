package com.madyclaire;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com.madyclaire")
@SpringBootApplication
public class MadyClaireMain {
	private static Logger log = Logger.getLogger(MadyClaireMain.class);
	public static void main(String[] args) throws Exception {
		ApplicationContext ctx = SpringApplication.run(MadyClaireMain.class, args);
		for (String bean : ctx.getBeanDefinitionNames()){
			log.info("Registered bean " + bean);
		}//end foreach bean
	}//end main
}//end class
   