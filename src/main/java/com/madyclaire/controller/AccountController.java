package com.madyclaire.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/account")
public class AccountController {
	@RequestMapping(method=RequestMethod.GET)
	public String viewAccount(Model model, RedirectAttributes redirectAttributes){
		if(model == null) {
			return "redirect:/account/login";
		}
		Map<String, Object> atts = model.asMap();
		if(atts.get("signedIn") == null || !(boolean) atts.get("signedIn")){
			return "redirect:/account/login";
		}
		return "account/account-details";
	}
	
	@RequestMapping(value="/login",method=RequestMethod.GET)
	public String login(){
		return "account/login-signup";
	}
	
	@RequestMapping(value="/register", method=RequestMethod.POST)
	public String register(@RequestParam("emailAddress") String emailAddress, RedirectAttributes redirectAttributes){
		redirectAttributes.addAttribute("statusMessage", "registered " + emailAddress);
		return "redirect:/";
	}
	
}
