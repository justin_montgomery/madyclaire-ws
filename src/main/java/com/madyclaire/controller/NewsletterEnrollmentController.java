package com.madyclaire.controller;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NewsletterEnrollmentController {
	private Logger log = Logger.getLogger(getClass());
	@RequestMapping(value="/enroll", method=RequestMethod.POST)
	public String enrollInNewsLetters(@RequestParam("emailAddress") String emailAddress){
		if(emailAddress == null || emailAddress.isEmpty() || !emailAddress.contains("@")){
			return "Please submit a valid email address...";
		}
		log.info(emailAddress + " attempted enrollment, but it is currently unsupported.");
		return emailAddress + " successfully enrolled to receive discounts.";
	}
}
