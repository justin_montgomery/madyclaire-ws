package com.madyclaire.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/shop")
public class ShoppingController {
	@RequestMapping(value="/clothes", method=RequestMethod.GET)
	public String getClothesInventory(){
		return "shopping/clothes";
	}//end getClothes
	@RequestMapping(value="/bedding", method=RequestMethod.GET)
	public String getBeddingInventory(){
		return "shopping/bedding";
	}//end get bedding
	@RequestMapping(value="/custom", method=RequestMethod.GET)
	public String getCustomAbilities(){
		return "shopping/custom";
	}//end getCustom
	
	@RequestMapping(value="/custom", method=RequestMethod.POST)
	public String requestCustomWork(Model model){
		
		model.addAttribute("customRequest", true);
		return "shopping/custom";
	}//end requestCustom
}//end class
