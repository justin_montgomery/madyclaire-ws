package com.madyclaire.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/blog")
public class BlogController {
	@RequestMapping(method=RequestMethod.GET)
	public String getAllPosts(){
		return "blog-list";
	}//end getAllPosts
	
	@RequestMapping(value="/{blog-id}", method=RequestMethod.GET)
	public String getBlogPost(Model model){
		return "blog";
	}//end getBlogPost
	
	@RequestMapping(value="/123", method=RequestMethod.GET)
	public String getBlogPost(){
		return "blog";
	}
}
